/* MIT License

Copyright (c) 2021 Javier Cañadillas (javier@canadillas.org)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

/*
Create a new Gitlab repo with a set of simplified options:
name, description and private flag
*/
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/xanzy/go-gitlab"
)

var (
	name        = flag.String("name", "", "Repo name")
	description = flag.String("description", "", "Repo description")
	private     = flag.Bool("private", false, "Mark repo as private")
	glVis       gitlab.VisibilityValue
)

func main() {
	flag.Parse()
	tokenEnvVar := "GITLAB_AUTH_TOKEN"
	token := os.Getenv(tokenEnvVar)

	// Check that basic flags are provided
	if token == "" {
		log.Fatalf("No token is present, please set the environment %v var and try again.", tokenEnvVar)
	}
	if *name == "" {
		log.Fatal("Repo name not provided, you must specify one.")
	}

	glClient, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	if *private {
		glVis = gitlab.VisibilityValue(gitlab.PrivateVisibility)
	} else {
		glVis = gitlab.VisibilityValue(gitlab.PublicVisibility)
	}

	// Create a new project
	p := &gitlab.CreateProjectOptions{
		Name:                 gitlab.String(*name),
		Description:          gitlab.String(*description),
		MergeRequestsEnabled: gitlab.Bool(true),
		SnippetsEnabled:      gitlab.Bool(true),
		Visibility:           &glVis,
	}

	project, _, err := glClient.Projects.CreateProject(p)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Successfully created new repo %v\n", project.Name)
}
