# Create GitLab Project CLI

This repo contains the source code and instructions to create a binary to automate projects creation in GitLab.

For it to work, you'll need a GitLab account and to generate a new access token from the [Personal Access Tokens section](https://gitlab.com/-/profile/personal_access_tokens). Once created, copy the token and store it in a shell var called `GITLAB_AUTH_TOKEN` that the binary will be reading.\

You will also need to have Golang installed in your system.

## Testing the code

Clone the repo and set up and run `main.go`: 

```bash
go run main.go -name=gitlab-newrepo -description="Sample Go Code on how to create GitLab repos from CLI"
```

## Building the binary

Build a binary from source (using `glcp` as name, which stands for **G**it**L**ad **C**reate **P**project):

```bash
go build -o glcp
```

and put the binary in a location reacheable by your `$PATH`. Then, invoke it:

```bash
glcp -h
glcp -name=gitlab-testproject -description="A private test project from CLI" -private
```